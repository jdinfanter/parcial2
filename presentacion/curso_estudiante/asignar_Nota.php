<?php
$idEstudiante = "";
if(isset($_POST["idEstudiante"])){
    $idEstudiante = $_POST["idEstudiante"];
}
$idCurso = "";
if(isset($_POST["idCurso"])){
    $idCurso = $_POST["idCurso"];
}
  
$nota = "";
if(isset($_POST["nota"])){
    $nota= $_POST["nota"];
}

if(isset($_POST["crear"])){
    $curso_Estudiante = new Curso_Estudiante($idEstudiante, $idCurso,"","","","",$nota);
    $curso_Estudiante -> insertar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Aasignar Nota</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/curso_estudiante/asignar_Nota.php") ?>" method="post">
						<div class="form-group">
							<label>Ingrese el id del estudiante</label> 
							<input type="number" name="idEstudiante" min="0" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Ingrese el id del Curso</label> 
							<input type="number" name="idCurso" min="0" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Ingrese la Nota: (de 0 a 5)</label> 
							<input type="number" name="nota" min="0" max="5" class="form-control"  required>
						</div>
						<button type="submit" name="crear" class="btn btn-dark">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>