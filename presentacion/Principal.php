<nav class="navbar navbar-expand-lg bg-dark navbar-dark sticky-top">
  <a class="navbar-brand" href="#">Parcial 2</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Estudiante
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/crearEstudiante.php")?>">Ingresar Estudiante</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/estudiante/consultarEstudiante.php")?>">Consultar Estudiante</a>
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Curso
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/curso/crearCurso.php")?>">Ingresar Curso</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/curso/consultarCurso.php")?>">Consultar Cursos</a>
          </div>
      </li>
      
    </ul>
  </div>
</nav>