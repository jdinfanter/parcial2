<?php
$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$apellido = "";
if(isset($_POST["apellido"])){
    $apellido = $_POST["apellido"];
}
  
if(isset($_POST["crear"])){
    $estudiante = new Estudiante("", $nombre,$apellido);
    $estudiante -> insertar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Insertar Estudiante</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/estudiante/crearEstudiante.php") ?>" method="post">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control"  required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control"  required>
						</div>
						<button type="submit" name="crear" class="btn btn-dark">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>