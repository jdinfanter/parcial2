<?php
class CursoDAO{
    private $idCurso;
    private $nombre;
    private $creditos;
       
    public function CursoDAO($idCurso = "", $nombre = "", $creditos= ""){
        $this -> idCurso = $idCurso;
        $this -> nombre = $nombre;
        $this -> creditos= $creditos;
    }

    public function consultar(){
        return "select nombre, creditos
                from curso
                where idCurso = '" . $this -> idCurso .  "'";
    }    
    
    public function insertar(){
        return "insert into curso (nombre,creditos)
                values ('" . $this -> nombre . "', '" . $this -> creditos . "')";
    }
    
    public function consultarTodos(){
        return "select idCurso, nombre, creditos
                from curso";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select idCurso, nombre, creditos
                from curso
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(idCurso)
                from curso";
    }
 
    
}

?>