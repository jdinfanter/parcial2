<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CursoDAO.php";
class Curso{
    private $idCurso;
    private $nombre;
    private $creditos;
    private $conexion;
    private $cursoDAO;
    
    public function getIdCurso(){
        return $this -> idCurso;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getCreditos(){
        return $this -> creditos;
        
    }
    
    
    public function Curso($idCurso = "", $nombre = "", $creditos= ""){
        $this -> idCurso = $idCurso;
        $this -> nombre = $nombre;
        $this -> creditos= $creditos;
        $this -> conexion = new Conexion();
        $this -> cursoDAO = new CursoDAO($this -> idCurso, $this -> nombre, $this -> creditos);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cursoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> creditos = $resultado[1];
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cursoDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cursoDAO -> consultarTodos());
        $cursos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Curso($resultado[0], $resultado[1], $resultado[2]);
            array_push($cursos, $c);
        }
        $this -> conexion -> cerrar();        
        return $cursos;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> cursoDAO -> consultarPaginacion($cantidad, $pagina));
        $cursos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Curso($resultado[0], $resultado[1], $resultado[2]);
            array_push($cursos, $c);
        }
        $this -> conexion -> cerrar();
        return $cursos;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->  cursoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    

    
    
}

?>