<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Curso_EstudianteDAO.php";
class Curso_Estudiante{
    private $idEstudiante;
    private $nombreEstudiante;
    private $apellidoEstudiante;
    private $idCurso;
    private $nombreCurso;
    private $creditos;
    private $nota;
    private $conexion;
    private $curso_estudianteDAO;
    
    public function getIdEstudiante(){
        return $this -> idEstudiante;
    }
    
    public function getNombreEstudiante(){
        return $this -> nombreEstudiante;
    }
    
    public function getApellidoEstudiante(){
        return $this -> apellidoEstudiante;
    }
    
    public function getIdCurso(){
        return $this -> idCurso;
    }
    
    public function getNombreCurso(){
        return $this -> nombreCurso;
    }
    
    public function getCreditos(){
        return $this -> creditos;
    }

    public function getNota(){
        return $this -> nota;
        
    }
    
    
    public function Curso_Estudiante($idEstudiante="",$idCurso = "",$nombreEstudiante="",$apellidoEstudiante="",$nombreCurso="",$creditos="" , $nota = ""){
        $this -> idEstudiante = $idEstudiante;
        $this -> nombreEstudiante = $nombreEstudiante;
        $this -> apellidoEstudiante = $apellidoEstudiante;
        $this -> nombreCurso = $nombreCurso;
        $this -> creditos = $creditos;
        $this -> idCurso = $idCurso;
        $this -> nota= $nota;
        $this -> conexion = new Conexion();
        $this -> curso_estudianteDAO = new Curso_EstudianteDAO($this -> idEstudiante, $this -> idCurso, $this ->nombreEstudiante, $this ->apellidoEstudiante,$this ->nombreCurso , $this ->creditos, $this -> nota);
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> curso_estudianteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombreEstudiante = $resultado[1];
        $this -> apellidoEstudiante = $resultado[2];
        $this -> nombreCurso = $resultado[4];
        $this -> creditos = $resultado[5];
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> curso_estudianteDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> curso_estudianteDAO -> consultar());
        $cursos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Curso_Estudiante($resultado[0], $resultado[3], $resultado[1],$resultado[2],$resultado[4],$resultado[5],$resultado[6],$resultado[7]);
            array_push($cursos, $c);
        }
        $this -> conexion -> cerrar();        
        return $cursos;
    }
    
    
}

?>