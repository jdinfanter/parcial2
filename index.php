<?php
require_once "logica/Estudiante.php";
require_once "logica/Curso.php";
require_once "logica/Curso_Estudiante.php";
$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);
}
?>
<html>
<head>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.	1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
	<?php 
	
	if($pid=="")
	{
	    include "presentacion/Principal.php";
	}else {
	    include "presentacion/Principal.php";
	    include $pid;
	}
	?>	
</body>
</html>