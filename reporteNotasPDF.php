<?php
require_once "logica/Curso_Estudiante.php";
require_once "ezpdf/class.ezpdf.php";

$pdf = new Cezpdf("LETTER");
$pdf -> selectFont("ezpdf/fonts/Courier.afm");
$pdf -> ezSetCmMargins(2, 2, 3, 3);

$curso_estudiante = new Curso_Estudiante();
$notas = $curso_estudiante -> consultarTodos();

$opciones = array("justification" => "center");
$pdf -> ezText("<b>Parcial 2</b>", 20, $opciones);
$pdf -> ezText("<b>Reporte Notas</b>", 16, $opciones);

$encabezados = array(
    "num" => "<b>#</b>",
    "nombre" => "<b>Estudiante</b>",
    "curso" => "<b>Curso</b>",
    "credito" => "<b>Creditos</b>",
    "nota" => "<b>Nota</b>",
);
$datos = array();
$i = 0;
    foreach ($notas as  $notaActual){
        $datos[$i]["num"] = $i + 1;
        $datos[$i]["nombre"] = $notaActual -> getNombreEstudiante() ." ". $notaActual -> getApellidoEstudiante() ;
        $datos[$i]["curso"] = $notaActual -> getNombreCurso();
        $datos[$i]["credito"] = $notaActual -> getCreditos();
        $datos[$i]["nota"] = $notaActual -> getNota();
        $i++;
    }    


$opcionesTabla = array(
    "showLines" => 1,
    "shaded" => 1,
    "rowGap" => 3
);
$pdf -> ezSetDY(-20);
$pdf -> ezTable($datos, $encabezados, "Reporte de Notas", $opcionesTabla);

$pdf -> ezStream();
// $pdfcode = $pdf->ezOutput();
// $fp=fopen("reportes/clientes.pdf",'wb');
// fwrite($fp,$pdfcode);
// fclose($fp);

?>